#!/usr/bin/env bash

export DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

# Optional step - it takes couple of seconds (or longer) to establish a WiFi connection
# sometimes. In this case, following checks will fail and wifi-connect
# will be launched even if the device will be able to connect to a WiFi network.
# If this is your case, you can wait for a while and then check for the connection.
sleep 30

# Choose a condition for running WiFi Connect according to your use case:

# 1. Is there a default gateway?
# ip route | grep default

# 2. Is there Internet connectivity?
# nmcli -t g | grep full

# 3. Is there Internet connectivity via a google ping?
# wget --spider http://google.com 2>&1

# 4. Is there an active WiFi connection?
# iwgetid -r

# 5. Is wifi-repeater enabled?

# TODO: Use CHECK_CONN_FREQ to check if network state changed?
# if [[ ! -z $CHECK_CONN_FREQ ]] 
#     then
#         freq=$CHECK_CONN_FREQ
#     else
#         freq=120
# fi


# sleep 5


# check if REPEATER_SSID is set and larger then 1
length=$(expr length "$PORTAL_SSID")
# if [ $? -eq 0 ]; then
if [ $length -gt 0 ]; then
    # Check for repeater 
    printf 'Starting WiFi\n'
    ./wifi-connect
    # Let Internet pass thru
    iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
    netfilter-persistent save
    # Start your application here.
    printf 'Starting repeater API\n'
    node build
    # sleep infinity
else
    printf 'Skipping WiFi because PORTAL_SSID was not set\n'
fi

# V1
# IF AP_NAME
#   IF REPEATER_NAME && SECONDARY_WIFI
#       DO WIFI-REPEATER
#       ON-OK exit
#       ON-FAIL
#       DO ERROR-NOTIFICATION
# ELSE
#   IF ETH
#       DO WIFI-AP
#       ON-OK exit
#   ELSE
#       DO WIFI-CONNECT
#       ON-OK exit

# V2
# IF config file
#   IF repeater ssid
# Start WIFI-REPEATER repeater
#   ELSE
# Start WIFI-REPEATER access-point (find captive-portal)
# ELSE
# Start WIFI-CONNECT