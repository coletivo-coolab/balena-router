import blinking from 'blinking';
import {
  connectToWifi,
  checkNMConnectivity,
  getWiFiDevices,
  getWiredDevices,
  createAccessPoint
} from "./nm";
// Import the express in typescript file
import express from 'express';
import cors from 'cors';

// LED notifications
const ledFile = '/sys/class/leds/led0/brightness';
const led = blinking(ledFile);
const LED_ERROR_PATTERNS = {
  NO_AP_CAPABLE_DEVICE: 2,
  NO_SECONDARY_WIRELESS: 3,
  NO_WIFI_CREDENTIALS: 4,
  NO_INTERNET: 5
};

// Initialize the express engine
const app: express.Application = express();
app.use(express.json());
app.use(cors());

// Take a port 3000 for running server.
const port: number = 4000;

// Handling '/' Request
app.get('/bridge', async (_req, _res) => {
  const wifiDevices = await getWiFiDevices();
  const accessPoint = wifiDevices.find(device => device.apCapable);
  const bridge = wifiDevices.find(device => device.iface !== accessPoint?.iface);
	_res.json(bridge || false);
});

app.get('/wifidevices', async (_req, _res) => {
  const wifiDevices = await getWiFiDevices();
	_res.json(wifiDevices);
});

app.get('/wireddevices', async (_req, _res) => {
  const wiredDevices = await getWiredDevices();
	_res.json(wiredDevices);
});

app.post('/ap', async (_req, _res) => {
  console.log('BODY', _req.body)
  // { iface, ssid, password }
  await createAccessPoint(_req.body);
})

app.post('/repeat', async (_req, _res) => {
  console.log('BODY', _req.body)
  await connectToWifi(_req.body);
  let nmConnected = await checkNMConnectivity();
  if (!nmConnected) {
    console.log(`Warning: Could not detect internet access. Bad WiFi credentials provided or WiFi network has no internet access...`);
    led.pattern.start({ blinks: LED_ERROR_PATTERNS.NO_INTERNET, pause: 1000 });
  }
	_res.json(nmConnected);
});

// Server setup
app.listen(port, () => {
	console.log(`TypeScript with Express
		http://localhost:${port}/`);
});